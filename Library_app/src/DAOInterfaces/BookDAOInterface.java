/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOInterfaces;

import DTO.*; 
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author edricajasmine
 */
public interface BookDAOInterface {
    
    public int addBook(User activeUser,Book book_details);
    public Book searchBookByID(User activeUser,int book_id);
    public double loanABook(User activeUser, int book_id);
    public int removeBook(User activeUser, int book_id);
    public int updateCopiesOfBook(User activeUser, int book_id, int newQuantity);
    public Book searchBookByTitle(User activeUser, String title);
    public int editBookDetails(User activeUser, Book book, HashMap<Integer,String> updatedDetails);
    public double getOverdueFee(User activeUser, int book_id);
    public ArrayList<Book> seeAllBooks(User activeUser);
    public ArrayList<Book> seeAllBooksByCustomer();
}
