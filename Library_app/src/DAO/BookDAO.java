
package DAO;

import DTO.*;
import DAOInterfaces.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.*;

/**
 *
 * @author edricajasmine
 */
public class BookDAO extends DAO implements BookDAOInterface{
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private Connection conn = null;
    private int resultCount;

    /**
     * Default constructor
     */
    public BookDAO(){    
    }
    
    /**
     * This method is for using the dummy database to test methods
     * @param dbName the dummy database
     */
    public BookDAO(String dbName){
        super(dbName);
    }
    
    /**
     * This method is used for adding a book to the library_system database
     * @param activeUser Object User to make sure the user adding a book title is either an admin or an owner
     * @param book_details Object book details to be added in the database
     * @return a String if a book is added successfully or not
     */
    @Override
    public int addBook(User activeUser,Book book_details){
        if(activeUser.getUserType() == 0 || activeUser.getUserType() == 1){
            try {
                conn = connect();

                ps = conn.prepareStatement("call addBook(?,?,?,?,?,?,?)");
                
                ps.setString(1,book_details.getTitle());
                ps.setString(2,book_details.getDescription());
                ps.setString(3,book_details.getAuthor());
                ps.setString(4,Date.valueOf(book_details.getRelease_date()));
                ps.setInt(5,book_details.getQuantity());
                ps.setInt(6, book_details.getLoan_time());
                ps.setDouble(7, book_details.getOverdue_fee());
                
                resultCount = ps.executeUpdate();
                
                super.closeConnections(conn, ps, rs);
                return resultCount;
                
            } catch (ClassNotFoundException | SQLException ex) {
                return -1;
            }
        }
        else 
            return -2;
    }
    
    /**
     * This method is used to see a single book detail
     * @param activeUser determines the user type which should be an admin = 1 
     * @param book_id is the book reference number 
     * @return an object book of the searching title
     */
    @Override
    public Book searchBookByID(User activeUser,int book_id){
        Book book = new Book();
        if(activeUser.getUserType() == 0 || activeUser.getUserType() == 1){
            try {
                conn = connect();

                ps = conn.prepareStatement("call searchBookAdmin(?)");
                ps.setInt(1,book_id);
                
                rs = ps.executeQuery();
                
                if(rs.next()){
                    book.setBook_id(rs.getInt("book_id"));
                    book.setTitle(rs.getString("title"));
                    book.setAuthor(rs.getString("author"));
                    book.setDescription(rs.getString("description"));
                    book.setRelease_date(rs.getString("release_date"));
                    book.setQuantity(rs.getInt("quantity"));
                    book.setLoan_time(rs.getInt("loan_time"));
                    book.setOverdue_fee(rs.getDouble("overdue_fee"));
                    
                    super.closeConnections(conn, ps, rs);
                    return book;
                }

            } catch (ClassNotFoundException | SQLException ex) {
                return book;
            }
        }
        return book;
    }
    
    /**
     * This method is used to validate objects to create a loan
     * @param activeUser receives the type of user loaning the book
     * @param book_id is the specific reference number of the book to loan
     * @return an object of the book found, otherwise returns null
     */
    @Override
    public double loanABook(User activeUser, int book_id){
        ActiveLoanDAO  connectLoanDAO = new ActiveLoanDAO();
        
        LocalDate date = LocalDate.now();
        UserDAO connectUserDAO = new UserDAO();
        double result = 0;
        Book book = new Book();
        Loan loan = new Loan();
        
        //validates if the userType is not a customer 
        if(activeUser.getUserType() == 0 || activeUser.getUserType() == 1){
            //calls the method to check if the userType has outstanding overdue fee, if so cannot loan a book
            double overdueFee = connectUserDAO.hasUserOverFee(activeUser);
            if(overdueFee>0){
                //calls the method to search and view a single book details and returns the book object
                book = searchBookByID(activeUser,book_id);
                //checks if the book is not null to proceed to the process of loaning a book
                if(!book.equals(null)){
                    //checks the number of books currently in loan
                    if(connectLoanDAO.countActiveLoanByBookID(book_id) < book.getQuantity()){
                        
                    loan = new Loan();
                    
                    loan.setBook(searchBookByID(activeUser, book_id));
                    
                    UserDAO user = new UserDAO();
                    loan.setCust(user.getUserById(activeUser.getUserId()));
                    
                    loan.setDate_start(date);
                    
                    loan.setDate_end(date.plusDays(book.getLoan_time()));
                    //calls a method to create loan in the database 
                    result = connectLoanDAO.createLoan(loan);
                    
                    return result;
                    }
                    //no available books
                    else 
                        return 3;
                }
                //book not found
                else
                    return 2;
            }
            //existing custsomer overdue fee, not allowed to loan any book until paid
            else {
                result = overdueFee;
                return result;
            }
        }
        //user doesn't have priviledges to this method
        return 4;
    }
    
    /**
     * This method does the process on removing a book, if conditions wasn't met, then remove book would be successful 
     * @param activeUser the current user of the app
     * @param book_id is the reference book number of a book
     * @return a specific integer base on the result of removing a book
     */
    @Override
    public int removeBook(User activeUser, int book_id){
        ActiveLoanDAO connectLoanDAO = new ActiveLoanDAO();
        Book book = new Book();
        Loan loan = new Loan();
        int result = 0;
        
        //checks if user type is not a customer
        if(activeUser.getUserType() == 0 || activeUser.getUserType() == 1){
            //calls method to search the book_id and returns the book object
            book = searchBookByID(activeUser, book_id);
            //checks if the object is not empty to process with the process
            if( book != null){
                //check if the books are currently in loan
                if(connectLoanDAO.countActiveLoanByBookID(book_id) == 0){
                    try {
                        conn = connect();
                        ps = conn.prepareStatement("call removeBook(?)");
                        ps.setInt(1,book_id);

                        resultCount = ps.executeUpdate();
                        
                        super.closeConnections(conn, ps, rs);
                        return resultCount;

                    } 
                    catch (ClassNotFoundException | SQLException ex) {
                        return 0;
                    }
                }
                //Book is currently in active loan, cannot remove a title
                else
                    return 3;
            }
            //book_id doesn't exist
            else
                return 2;
        }
        //user doesn't have priviledges to this method
        return 4;
    }
    
    /**
     * This method is used to validate the transaction then update the number of copies in the book
     * 
     * @param activeUser is the object of the current user
     * @param book_id is the reference book number
     * @param newQuantity is the number of copies to be updated 
     * @return an integer depending on the result 
     */
    @Override
    public int updateCopiesOfBook(User activeUser, int book_id, int newQuantity){
        ActiveLoanDAO connectLoanDAO = new ActiveLoanDAO();
        int result = 0;
        Book book = new Book();
        
        if(activeUser.getUserType() == 0 || activeUser.getUserType() == 1)   {
            //calls method to search and return the book
            book = searchBookByID(activeUser, book_id);
            //checks if the book returned is null
            if(book != null)    {
                //if newQuantity is lesser than the current quantity at the library
                if(newQuantity < book.getQuantity())    {
                    //checks the newQuantity to the number of copies currently on loan
                    if(connectLoanDAO.countActiveLoanByBookID(book_id) <= newQuantity)  {
                        try {
                            conn = connect();
                            ps = conn.prepareStatement("call updateCopiesOfBook(?,?)");
                            ps.setInt(1,book_id);
                            ps.setInt(2,newQuantity);

                            resultCount = ps.executeUpdate();

                            super.closeConnections(conn, ps, rs);
                            return resultCount;
                        } 
                        catch (ClassNotFoundException | SQLException ex) {
                            return 0;
                        }
                    }
                    //newQuantity is lesser than the number of copies in activeLoan, retrieve the copies first then update again.
                    else
                        return 3;
                }
                //update
                else {
                    book.setQuantity(newQuantity);
                    return 1;
                }
            }
            //book doesn't exist
            else 
                return 2;
        }
        //user doesn't have priviledges to this method
        return 4;
    }

    /**
     * This method is used to retrieve a book in search by a title
     * @param activeUser is userType 
     * @param title is the name of the title in search
     * @return a book object, null if the book is not found, otherwise with its details
     */
    @Override
    public Book searchBookByTitle(User activeUser, String title){
        Book book = new Book();
        if(activeUser.getUserType() == 0 || activeUser.getUserType() == 1){
            try {
                conn = connect();

                ps = conn.prepareStatement("call searchBookByTitle(?)");
                ps.setString(1,title);
                
                rs = ps.executeQuery();
                
                if(rs.next()){
                    book.setBook_id(rs.getInt("book_id"));
                    book.setTitle(rs.getString("title"));
                    book.setAuthor(rs.getString("author"));
                    book.setDescription(rs.getString("description"));
                    book.setRelease_date(rs.getString("release_date"));
                    book.setQuantity(rs.getInt("quantity"));
                    book.setLoan_time(rs.getInt("loan_time"));
                    book.setOverdue_fee(rs.getDouble("overdue_fee"));
                    
                    super.closeConnections(conn, ps, rs);
                    return book;
                }

            } catch (ClassNotFoundException | SQLException ex) {
                return book;
            }
        }
        return book;
    }
    
    /**
     * This method is used to validate the user's request to edit a book's details
     * @param activeUser the user Type handling the request
     * @param book the object of the book requesting to update
     * @param updatedDetails is a HashMap that contains the new details of the book
     * @return an integer which represents an user friendly message from the caller
     */
    @Override
    public int editBookDetails(User activeUser, Book book, HashMap<Integer,String> updatedDetails){
        Set set = updatedDetails.entrySet();
        Iterator item = set.iterator();
        Book compareBook = new Book();
       
        if(activeUser.getUserType() == 0 || activeUser.getUserType() == 1){
            while(item.hasNext()){
                Map.Entry newDetails = (Map.Entry)item.next();
                int menu = (int) newDetails.getKey();
                switch(menu){
                    case 1:
                        //calls a method to search the new title to the existing ones to make sure no same values 
                        compareBook = searchBookByTitle(activeUser, updatedDetails.get(menu));
                        //checks if compareBook is not null, if so, can update with no problem
                        if(compareBook != null){
                            if(compareBook.getAuthor().equalsIgnoreCase(book.getAuthor())){
                                //-2 to indicate the new details would duplicate an existing book in the library
                                return -2;
                            }
                            //can update
                            else {
                                book.setTitle(updatedDetails.get(menu));
                            }
                        }
                        //can update
                        else {
                            book.setTitle(updatedDetails.get(menu));
                        }
                        break;
                    case 2:
                        //calls a method to search the title to the existing ones to make sure no same values 
                        compareBook = searchBookByTitle(activeUser, book.getTitle());
                        //checks if compareBook is not null, if so, can update with no problem
                        if(compareBook != null){
                            if(compareBook.getAuthor().equalsIgnoreCase(book.getAuthor())){
                                //-1 to indicate the new details would duplicate an existing book in the library
                                return -2;
                            }
                            //can update
                            else {
                                book.setAuthor(updatedDetails.get(menu));
                            }
                        }
                        //can update
                        else {
                            book.setAuthor(updatedDetails.get(menu));
                        }
                        break;
                    case 3:
                        book.setDescription(updatedDetails.get(menu));
                        break;
                    case 4:
                        book.setRelease_date(updatedDetails.get(menu));
                        break;
                    case 5:
                        int quantity = Integer.parseInt(updatedDetails.get(menu));
                        book.setQuantity(quantity);
                        break;
                    case 6:
                        int loan_time = Integer.parseInt(updatedDetails.get(menu));
                        book.setLoan_time(loan_time);
                        break;
                    case 7:
                        double fee = Double.parseDouble(updatedDetails.get(menu));
                        book.setOverdue_fee(fee);
                        break;
                }
            }
        }
        else
            return 2;
       
        try {
            conn = connect();

            ps = conn.prepareStatement("call editBookDetails(?,?,?,?,?,?,?,?)");

            ps.setInt(1,book.getBook_id());
            ps.setString(2,book.getTitle());
            ps.setString(3,book.getDescription());
            ps.setString(4,book.getAuthor());
            ps.setString(5,book.getRelease_date());
            ps.setInt(6,book.getQuantity());
            ps.setInt(7, book.getLoan_time());
            ps.setDouble(8, book.getOverdue_fee());

            resultCount = ps.executeUpdate();
            
            super.closeConnections(conn, ps, rs);
            return resultCount;

        } catch (ClassNotFoundException | SQLException ex) {
            return -1;
        }
    }
    
    /**
     * This method used to retrieve an overdue fee of a specific book
     * @param activeUser is the user requester
     * @param book_id is the reference book number
     * @return a double for the fee of the specific book
     */
    @Override
    public double getOverdueFee(User activeUser, int book_id){
        double result = 0;
        Book book = new Book();
        book = searchBookByID(activeUser,book_id);
        if(book != null){
            if(book.getBook_id() != 0){
                result = book.getOverdue_fee();
                return result;
            }
            else
                return -2;
        }
        else 
            return -1;
        
    }
    
    /**
     * This method is used to retrieve all books in the library
     * @param activeUser is the requestor either an owner or admin
     * @return an ArrayList of all books in the database
     */
    @Override
    public ArrayList<Book> seeAllBooks(User activeUser){
        ArrayList<Book> bookList = new ArrayList<Book>();
        Book book = new Book();
        
        if(activeUser.getUserType() == 0 || activeUser.getUserType() == 1){
            try {
                conn = connect();

                ps = conn.prepareStatement("call seeAllBooks()");

                rs = ps.executeQuery();
                
                while(rs.next()){
                    book.setBook_id(rs.getInt("book_id"));
                    book.setTitle(rs.getString("title"));
                    book.setAuthor(rs.getString("author"));
                    book.setDescription(rs.getString("description"));
                    book.setRelease_date(rs.getString("release_date"));
                    book.setQuantity(rs.getInt("quantity"));
                    book.setLoan_time(rs.getInt("loan_time"));
                    book.setOverdue_fee(rs.getDouble("overdue_fee"));
                    
                    bookList.add(book);
                }
                super.closeConnections(conn, ps, rs);
                return bookList;

            } catch (ClassNotFoundException | SQLException ex) {
                return null;
            }
        }
        return bookList;
    }
    
    
    /**
     * This is used to retrieve all titles in the library but with limited 
     * @return an array list of books in the library
     */
    @Override
    public ArrayList<Book> seeAllBooksByCustomer(){
        ArrayList<Book> bookList = new ArrayList<Book>();
        Book book = new Book();
        
        try {
            conn = connect();

            ps = conn.prepareStatement("call seeAllBooks()");

            rs = ps.executeQuery();

            while(rs.next()){
                book.setBook_id(rs.getInt("book_id"));
                book.setTitle(rs.getString("title"));
                book.setAuthor(rs.getString("author"));
                book.setDescription(rs.getString("description"));
                book.setRelease_date(rs.getString("release_date"));

                bookList.add(book);
                
            }
            super.closeConnections(conn, ps, rs);
            return bookList;

        } catch (ClassNotFoundException | SQLException ex) {
            return null;
        }
    }
        
}