package DAO;

import DAOInterfaces.ActiveLoanDAOInterface;
import DTO.Book;
import DTO.Loan;
import DTO.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.sql.Date;
import static java.time.temporal.ChronoUnit.DAYS;
import java.util.ArrayList;


/**
 *
 * @author Maria McCahey
 */
public class ActiveLoanDAO extends DAO implements ActiveLoanDAOInterface {

    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private Connection conn = null;
    private int resultCount;

    //  Constructor for live database
    public ActiveLoanDAO() {
    }

    /**
     * Constructor for testing
     *
     * @param dbName String testing database name
     */
    public ActiveLoanDAO(String dbName) {
        super(dbName);
    }

    /**
     *
     * @param newLoan
     * @return Create a new loan for existing customer and existing book
     * 
     */
    
   
    @Override
    public int createLoan(Loan newLoan) {
        
        
        try {
            conn = connect();// Connect to database

            ps = conn.prepareStatement("call createLoan(?,?,?,?)");// prepare statement to deal with query in database
       
            ps.setInt(1, newLoan.getCust().getUserId()); // set user id to new loan
            ps.setInt(2, newLoan.getBook().getBook_id());// set book id to new loan
            ps.setDate(3, Date.valueOf(newLoan.getDate_start()));// set start date to new loan (date loan was taken out)
            ps.setDate(4, Date.valueOf(newLoan.getDate_end()));// set end date to new loan ( date the loan is to be returned)
        
           
            resultCount = ps.executeUpdate(); // executeUpdate for an insert, update or delete query. The results of the above loan set go into resultCount
            
            super.closeConnections(conn, ps, rs);// we need to close our connections 
            
            return resultCount;// we return the new loan details in resultCount
       
           // Catch exception for errors eg sql error or invalid input    
        } catch (ClassNotFoundException | SQLException ex) {
           return 2;
        }
    }
    
    /**
     * 
     * @param activeUser
     * @param book_id
     * @param user_id
     * @return returns active loan
     */
 
    @Override
    public Loan getActiveLoan(User activeUser, int book_id, int user_id) {
        // we are asking if the user id of the logged in user is the same as the user id of the user that has the book on loan 
        // OR if the logged in user is user type 1 or 2 - that is owner or admin
        if (activeUser.getUserId() == user_id || activeUser.getUserType() == 1 || activeUser.getUserType() == 0) {
         
            try {
                conn = connect();// Connect to database

                ps = conn.prepareStatement("call getActiveLoan(?,?)");// prepare statement to deal with query in database

                ps.setInt(1, user_id);
                ps.setInt(2, book_id);
                
                rs = ps.executeQuery(); //use executeQuery for select statements

                if (rs.next()) { // see if there is information
                    Loan ln = new Loan(); // create a new loan object
                    int loanId = rs.getInt("loan_id"); 
                    
                    UserDAO customer = new UserDAO();
                    User cust = customer.getUserById(rs.getInt("user_id")); // this line is going to retrieve a customer
                    BookDAO book = new BookDAO();
                    Book Bk = book.searchBookByID(activeUser,rs.getInt("book_id"));// this is going to retrieve a book that is on loan by the active user
                    LocalDate start = rs.getDate("date_start").toLocalDate(); // retrieves loan start date
                    LocalDate end = rs.getDate("date_end").toLocalDate();// retrieves loan end date

                    ln.setCust(cust); // set customer to the loan
                    ln.setLoan_id(loanId); // set loan id to the loan
                    ln.setBook(Bk); // set book to the loan
                    ln.setDate_start(start); // set start date to the loan
                    ln.setDate_end(end); // set end date (return date) to the loan
                    return ln; // the loan object with all the above information in it
                }
                super.closeConnections(conn, ps, rs); // close our connection
                return new Loan(); // returns the loan details
                // Catch exception for errors eg sql error or invalid input
            } catch (ClassNotFoundException | SQLException ex) {
                return null;

            }

        }
        return null;
    }
    /**
     * Its to count the number of book of the same name in the active loan table
     * @param book_id is the condition to match the book
     * @return  number of loans
     */
    
    @Override
    public int countActiveLoanByBookID(int book_id) {

        try {
            conn = connect(); // connect to database
            ps = conn.prepareStatement("call countActiveLoanByBookID(?)"); // prepare statement to deal with query in database

            ps.setInt(1, book_id); // set book id to be used for the query
            rs = ps.executeQuery(); //use executeQuery for select statements

            int count = rs.getInt("numLoans"); // the number of loans are stored in count
            super.closeConnections(conn, ps, rs); // close the connection
            return count; // returns count ( number of loans)

        } 
         // Catch exception for errors eg sql error or invalid input
        catch (ClassNotFoundException | SQLException ex) {
            return -1; // -1 is an error

        }

    }
    
    /**
     * It's to count a customer's track on how many books is currently on active loan
     * @param user_id is id to reference he same user
     * @return an integer from resultCount
     */
    @Override
    public int countActiveLoanByCustomerId(int user_id) {

        try {
            conn = connect(); // connect to database
            ps = conn.prepareStatement("call countActiveLoanByUserID(?)"); // prepare statement to deal with query in database

            ps.setInt(1, user_id); // set user id to be used for the query
            rs = ps.executeQuery(); //use executeQuery for select statements

            int count = rs.getInt("numLoans"); // the number of loans are stored in count
            super.closeConnections(conn, ps, rs); // close the connection
            return count; // returns count ( number of loans)

        } 
         // Catch exception for errors eg sql error or invalid input
        catch (ClassNotFoundException | SQLException ex) {
            return -1; // -1 is an error

        }

    }
    /**
     * 
     * @param activeUser
     * @param user_id
     * @param book_id
     * @return the active loans
     */
    
    @Override
    public double returnActiveLoan(User activeUser, int user_id, int book_id){
        // we are asking if the user id of the logged in user is the same as the user id of the user that has the book on loan 
        // OR if the logged in user is user type 1 or 2 - that is owner or admin
        if (activeUser.getUserId() == user_id || activeUser.getUserType() == 1 || activeUser.getUserType() == 0) {
            BookDAO book = new BookDAO(); // create book object
            double bookFee = book.getOverdueFee(activeUser,book_id); // create bookFee to store the users overdue fees in relation to the book using the method getOverdueFee()
            
            if(bookFee >= 0){
                Loan loan1 = getActiveLoan(activeUser, user_id, book_id); // if bookFee is greater or equal to 0, get the active user, user id and book id and store in loan1 
              
                if(loan1 != null){ // if loan1 is not empty
                    if(loan1.getLoan_id()!= 0){ // if loan id of loan1 is not 0

                        LocalDate currentDate = loan1.getDate_end();// get end date of loan 1
                        LocalDate endDate = LocalDate.now();// at present dat(the date the book was return) .... used to see if book is overdue

                        // if book is overdue
                        if(currentDate.isAfter(endDate)){
                            long days = DAYS.between(currentDate, endDate); // calculate the number of days its overdue
                            double overDueFee = bookFee*days; // calculate overdue fee

                            UserDAO user = new UserDAO(); // create user object
                            int result = user.increaseUserOverFee(overDueFee); // put the overdue fees in result
                            switch(result){
                                case 0:
                                    // failed ....... rollback

                                    return 0;
                                case 1:
                                    // success ...... add loan to RecordLoan
                                    LoanRecordDAO loanRecord = new LoanRecordDAO();
                                    result = loanRecord.addNewLoanRecord(loan1, activeUser);
                                    switch(result){
                                        case 0:
                                            // failed ....... rollback
                                            return 0;
                                        case 1:
                                            // success ........ delete active loan
                                            return deleteActiveLoan(loan1);

                                        case 2:
                                            return -2;
                                    }
                                case 2:
                                    return -2;

                            }
                        }
                    }
                }
            }
            switch((int)bookFee){
                case -1:
                    return 0;
                default:
                    return bookFee; 
            }
        }
        
        return -4;
    }
    
    /**
     * Remove an active loan from the database.
     * Private method to avoid users from being able to delete them without doing any of the necessary checks
     * 
     * @param loan Loan which record is going to be remove
     * @return 1 if the loan was deleted successfully, 2 if there was an SQL error and 0 otherwise 
     */
    private int deleteActiveLoan(Loan loan){
        try {
            conn = connect(); // connect to database
            ps = conn.prepareStatement("call removeActiveLoan(?)"); // prepare statement to deal with query in database 

            ps.setInt(1, loan.getLoan_id()); // get the active loan by loan id
            super.closeConnections(conn, ps, rs);// close the connection
            return ps.executeUpdate();//return prepare statement to deal with the delete active loan

        }
        // Catch exception for errors eg sql error or invalid input
        catch (ClassNotFoundException | SQLException ex) {
            return 2; 
        }
    }
    
    @Override
    public ArrayList<Loan> seeAllActiveLoans(User activeUser){
        if (activeUser.getUserType() == 1 || activeUser.getUserType() == 0) {
            ArrayList<Loan> list = new ArrayList<Loan>();
            UserDAO connectUser = new UserDAO();
            BookDAO connectBook = new BookDAO();
            Loan loan = new Loan();

            try {
                conn = connect();

                ps = conn.prepareStatement("call seeAllLoans()");

                rs = ps.executeQuery();

                while(rs.next()){
                    loan.setLoan_id(rs.getInt("loan_id"));
                    loan.setCust(connectUser.getUserById(rs.getInt("user_id")));
                    loan.setBook(connectBook.searchBookByID(activeUser, rs.getInt("book_id")));
                    loan.setDate_start(rs.getDate("date_start").toLocalDate());
                    loan.setDate_end(rs.getDate("date_end").toLocalDate());
                    loan.setReturn_date(rs.getDate("return_date").toLocalDate());
                    list.add(loan);

                }
                super.closeConnections(conn, ps, rs);
                return list;

            } catch (ClassNotFoundException | SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return null;
    }

    @Override
    public ArrayList<Loan> seeAllActiveLoansByCustomer(User activeUser, int userId) {
        if (activeUser.getUserType() == 1 || activeUser.getUserType() == 0) {
            ArrayList<Loan> list = new ArrayList<Loan>();
            UserDAO connectUser = new UserDAO();
            BookDAO connectBook = new BookDAO();
            Loan loan = new Loan();

            try {
                conn = connect();

                ps = conn.prepareStatement("call seeAllLoansByCustomer(?)");
                
                ps.setInt(1,userId);
                
                rs = ps.executeQuery();

                while(rs.next()){
                    loan.setLoan_id(rs.getInt("loan_id"));
                    loan.setCust(connectUser.getUserById(rs.getInt("user_id")));
                    loan.setBook(connectBook.searchBookByID(activeUser, rs.getInt("book_id")));
                    loan.setDate_start(rs.getDate("date_start").toLocalDate());
                    loan.setDate_end(rs.getDate("date_end").toLocalDate());
                    loan.setReturn_date(rs.getDate("return_date").toLocalDate());
                    list.add(loan);

                }
                super.closeConnections(conn, ps, rs);
                return list;

            } catch (ClassNotFoundException | SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return null;
    }
}
