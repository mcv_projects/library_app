
package DTO;

import java.time.LocalDate;
import java.util.Objects;

/**
 *
 * @author Maria McCahey
 */
public class Loan {
    // create our variables
    private int loan_id;
    private User cust = new User();
    private Book book = new Book();
    private LocalDate date_start;
    private LocalDate date_end;
    private LocalDate return_date;
    private int overdue;
    
    
 public Loan() {
     
 }
    // create our getters and setters
    public int getLoan_id() {
        return loan_id;
    }
 
    public void setLoan_id(int loan_id) {
        this.loan_id = loan_id;
    }

    public User getCust() {
        return cust;
    }

    public void setCust(User cust) {
        this.cust = cust;
    }   
    
    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }    
    
    public LocalDate getDate_start() {
        return date_start;
    }

    public void setDate_start(LocalDate date_start) {
        this.date_start = date_start;
    }
    
    public LocalDate getDate_end() {
        return date_end;
    }
    
    public void setDate_end(LocalDate date_end) {
        this.date_end = date_end;
    }
     
    public LocalDate getReturn_date() {
        return return_date;
    }
    
     public void setReturn_date(LocalDate return_date) {
        this.return_date = return_date;
    }

    public int getOverdue() {
        return overdue;
    }
    
    public void setOverdue(int overdue) {
        this.overdue = overdue;
    }

    // create our hashCode method
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + this.loan_id;
        hash = 23 * hash + Objects.hashCode(this.cust);
        hash = 23 * hash + Objects.hashCode(this.book);
        hash = 23 * hash + Objects.hashCode(this.date_start);
        hash = 23 * hash + Objects.hashCode(this.date_end);
        hash = 23 * hash + this.overdue;
        return hash;
    }

    // create our equals method 
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Loan other = (Loan) obj;
        if (this.loan_id != other.loan_id) {
            return false;
        }
        if (this.overdue != other.overdue) {
            return false;
        }
        if (!Objects.equals(this.date_start, other.date_start)) {
            return false;
        }
        if (!Objects.equals(this.date_end, other.date_end)) {
            return false;
        }
        if (!Objects.equals(this.cust, other.cust)) {
            return false;
        }
        if (!Objects.equals(this.book, other.book)) {
            return false;
        }
        return true;
    }
    
    // create our toString method
    @Override
    public String toString() {
        return "\nLoan" + 
               "\nloan_ID: " + loan_id +
               "\ncust: " + cust +
               "\nbook: " + book +
               "\ndate_start: " + date_start +
               "\ndate_end: " + date_end +
               "\noverdue: " + overdue;
    }
    
 }