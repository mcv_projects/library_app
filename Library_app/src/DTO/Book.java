/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.time.LocalDate;
import java.util.Objects;

/**
 *
 * @author edricajasmine
 */
public class Book {
    private int book_id;
    private String title;
    private String description;
    private String author;
    private LocalDate release_date;
    private int quantity;
    private int loan_time;
    private double overdue_fee;

    public int getBook_id() {
        return book_id;
    }

    public void setBook_id(int book_id) {
        this.book_id = book_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public LocalDate getRelease_date() {
        return release_date;
    }

    public void setRelease_date(LocalDate release_date) {
        this.release_date = release_date;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getLoan_time() {
        return loan_time;
    }

    public void setLoan_time(int loan_time) {
        this.loan_time = loan_time;
    }

    public double getOverdue_fee() {
        return overdue_fee;
    }

    public void setOverdue_fee(double overdue_fee) {
        this.overdue_fee = overdue_fee;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + this.book_id;
        hash = 53 * hash + Objects.hashCode(this.title);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Book other = (Book) obj;
        if (this.book_id != other.book_id) {
            return false;
        }
        if (!Objects.equals(this.title, other.title)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "\nBook details " + 
                "\nBook ID: " + book_id + 
                "\nTitle: " + title + 
                "\nDescription: " + description + 
                "\nAuthor: " + author + 
                "\nRelease Date: " + release_date + 
                "\nQuantity: " + quantity + 
                "\nLoan: " + loan_time + 
                "\nOverdue fee: " + overdue_fee;
    }
    
    
    
}
