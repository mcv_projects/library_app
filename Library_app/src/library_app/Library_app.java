package library_app;


import DTO.*;
import DAO.*;
import java.util.*;
import ValidationInterfaces.ConsoleInputs;
import java.text.NumberFormat;

/**
 *
 * @author jorgevilasecojurado
 */
public class Library_app {
    
    static User activeUser = new User();
    static Book book = new Book();
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        boolean endProgram = false;

        while(!endProgram){
            //ownerMenu() displays the menu, and userOption receives the user input off the menu
            endProgram = mainMenu();
        }
    }
    


    private static int ownerMenu(){
        int userOption;
        int result;
        while(true){

            System.out.println("Welcome "+activeUser.getName());
            System.out.println("1) Create Admin account");
            System.out.println("2) Add Book");
            System.out.println("3) Edit Book Details");
            System.out.println("4) Update Book Quantity");
            System.out.println("5) Search book by Book ID");
            System.out.println("6) See all titles in the library");
            System.out.println("7) Loan a book by Book ID");
            System.out.println("8) See loan details");
            System.out.println("9) See all active loans");
            System.out.println("10) Remove a book from the library");
            System.out.println("11) Return a book");
            System.out.println("12) Delete a User");
            System.out.println("13) Logout");
            System.out.println("14) Exit");
            System.out.print("");

            userOption = ConsoleInputs.askForValidMenuOption("Input menu of choice: ",14);
                
            switch(userOption){
                case 1:
                    createAdmin();
                    break;
                case 2: 
                    addBook();
                    break;
                case 3:
                    editBookDetails();
                    break;
                case 4: 
                    updateBookQuantity();
                    break;
                case 5: 
                    searchBook(); 
                    break;
                case 6: 
                    seeAllBooks();
                    break;
                case 7: 
                    loanABook();
                    break;
                case 8:
                    seeSingleLoanDetails();
                    break;
                case 9:
                    seeAllActiveLoans();
                    break;
                case 10:
                    removeBook();
                    break;
                case 11:
                    returnBook();
                case 12:
                    removeUser();
                    break;
                case 13:
                    logout();
                    return 0;
                case 14:
                    return -1;
                default:
                    System.out.println("Insert a valid option");
            }
        }
    }
    /**
     * Create an Administrator account.
     * 
     * Display a console interface to get the data from the user, validates it
     * and make the call to the DAO to create the new user. it also checks if the user 
     * have the privileges to create this type of user
     * 
     */
    private static void createAdmin(){
        
        User newUser = new User();

        newUser.setName(ConsoleInputs.askForStringLargeThan("Enter admin name", 2));
        newUser.setSurname(ConsoleInputs.askForStringLargeThan("Enter admin surname", 2));
        newUser.setAddress1(ConsoleInputs.askForStringLargeThan("Enter address line 1", 2));
        newUser.setAddress2(ConsoleInputs.askForStringLargeThan("Enter address line 2", 2));
        newUser.setTown(ConsoleInputs.askForStringLargeThan("Enter city/town", 2));
        newUser.setCounty(ConsoleInputs.askForStringLargeThan("Enter county", 2));
        newUser.setPhone(ConsoleInputs.askForPhone("Enter contact number"));
        newUser.setEmail(ConsoleInputs.askForEmail("Enter email"));
        newUser.setPassword(ConsoleInputs.askForPassword("The password has to have:"+'\n'+"-Uppercase"+'\n'+"-Lowercase"+'\n'+"A number"+'\n'+"A special character @ # $ % "));

        UserDAO myConnection = new UserDAO();

        switch(myConnection.addAdmin(newUser,activeUser)){
            case 0:
                System.out.println("Admin user couldn't be created");
                break;
            case 1:
                System.out.println("Admin user created");
                break;
            case 2:
                System.out.println("Problems with the database");
                break;
            case 3:
                System.out.println("You don't have privilages to do this");
                break;
        }
    }
    
    private static int adminMenu(){
        int userOption;
        int result;
        
        while(true){

            System.out.println("Welcome "+activeUser.getName());
            System.out.println("1) Add Book");
            System.out.println("2) Edit Book Details");
            System.out.println("3) Update Book Quantity");
            System.out.println("4) Search book by Book ID");
            System.out.println("5) Loan a book");
            System.out.println("6) See loan details");
            System.out.println("7) Remove a book from the library");
            System.out.println("8) Return a book");
            System.out.println("9) Delete a user");
            System.out.println("10) See all active loans");
            System.out.println("11) See all loans made of a user");
            System.out.println("12) Logout");
            System.out.println("13) Exit");
            System.out.print("");

            userOption = ConsoleInputs.askForValidMenuOption("Input menu of choice: ",13);
                
            switch(userOption){
                case 1: 
                    addBook();
                    break;
                case 2:
                    break;
                case 3: 
                    updateBookQuantity();
                    break;
                case 4: 
                    searchBook(); 
                    break;
                case 5: 
                    loanABook();
                    break;
                case 6:
                    seeSingleLoanDetails();
                    break;
                case 7:
                    removeBook();
                    break;
                case 8:
                    returnBook();
                    break;
                case 9:
                    removeUser();
                    break;
                case 10:
                    seeAllActiveLoans();
                    break;
                case 11:
                    seeAllLoanByCustomer();
                    break;
                case 12:
                    logout();
                    return 0;
                case 13:
                    return -1;
                default:
                    System.out.println("Insert a valid option");
            }
        }
    }
    
    private static int customerMenu(){
        int userOption;
        int result;
        while(true){
            System.out.println("Welcome "+activeUser.getName());
            System.out.println("1) Loan a book");
            System.out.println("2) See all my loans");
            System.out.println("3) See all my current loans");
            System.out.println("4) Return a book");
            System.out.println("5) Logout");
            System.out.println("6) Exit");
            
            userOption = ConsoleInputs.askForValidMenuOption("Input menu of choice: ",6);
            switch(userOption){
                case 1:
                    loanABook();
                    break;
                case 2:
                    seeAllMyLoans();
                    break;
                case 3:
                    seeAllMyCurrentLoans();
                    break;
                case 4:
                    returnBook();
                    break;
                case 5:
                    logout();
                    return 0;
                case 6:
                    return -1;
                default:
                    System.out.println("Insert a valid option");
            }
        }
    }
    
    /**
     * This method is used to retrieve inputs needed from the user to add a book.
     */
    private static void addBook(){
        BookDAO connectBookDAO = new BookDAO();
        book = new Book();
        int result;
        
        System.out.println("New book details");
        book.setTitle(ConsoleInputs.askForString("New Title: "));
        book.setAuthor(ConsoleInputs.askForString("Description: "));
        book.setDescription(ConsoleInputs.askForString("Author: "));
        book.setRelease_date(ConsoleInputs.askForDate("Release date: "));
        book.setQuantity(ConsoleInputs.askForPositiveInt("Quantity: "));
        book.setLoan_time(ConsoleInputs.askForPositiveInt("Days to Loan: "));
        book.setOverdue_fee(ConsoleInputs.askForPositiveDouble("Overdue fee: "));
        
        result = connectBookDAO.addBook(activeUser, book);
        
        switch(result){
            case -1:
                System.out.println("Problems adding the book, try again later please");
                break;
            case 0:
                System.out.println("Adding a book couldn't be created");
                break;
            case 1:
                System.out.println("Book added successfully. Enjoy your book "+book.getTitle());
                break;
            default:
                System.out.println("You don't have privilages to do this");
        }
    }
    
    /**
     * This method is for user to have a console for search book by book id
     */
    private static void searchBook(){
        BookDAO connectBookDAO = new BookDAO();
        int book_id = 0;
        
        System.out.println("Search book by book ID ");
       
        book =  connectBookDAO.searchBookByID(activeUser,ConsoleInputs.askForInt("Book ID: "));
        
        if(book != null){
            System.out.println(book.toString());
        }
        else {
            System.out.println("Book id not found");
        }
    }
    
    /**
     * This method retrieves user inputs and calls a specific DAO method and receives a return of results and transcribe to a user friendly message
     */
    private static void loanABook(){
        BookDAO connectBookDAO = new BookDAO();
        double resultD=0, result=0;
        
        System.out.println("Loan a book by book ID");
        result = connectBookDAO.loanABook(activeUser, ConsoleInputs.askForInt("Book ID: "));
        
        if(resultD%1 != 0.00){
            System.out.println("Cannot allow to loan the book, customer has overdue fee of €" +resultD);
            System.out.println("Please pay fee first to loan a book.");
        }
        else {
            switch((int) resultD){
                case 0:
                    System.out.println("Could not loan a book");
                    break;
                case 1:
                    System.out.println("Book loaned successfully. Enjoy your book "+book.getTitle());
                    break;
                case 2:
                    System.out.println("Book was not found, please check book id.");
                    break;
                case 3: 
                    System.out.println("No available books at the moment");
                    break;
                case 4:
                    System.out.println("You don't have privilages to do this");
                    break;
            }
        }
    }
    /**
     * Main menu interface
     * 
     * @return true if user wants to continue in the program, false otherwise
     */
    private static boolean mainMenu(){
        int userOption;

        while(true){

            System.out.println("Welcome to our library");
            System.out.println("1) Login");
            System.out.println("2) Register");
            System.out.println("3) Exit");

            userOption = ConsoleInputs.askForValidMenuOption("Input menu of choice: ",3);
                
            switch(userOption){
                case 1:
                    
                    return login();
                    
                case 2:
                    register();
                    return false;
                case 3:
                    endProgram();
                    return true;
                default:
                    System.out.println("Insert a valid option");
                    return false;
            }
        }
    }
    /**
     * Make sure the program is close properly
     */
    private static void endProgram() {}
    /**
     * Login an user
     * 
     * @return the user type, -1 if not matches and -2 if an error occurred
     */
    private static boolean login() {
        UserDAO myConnection = new UserDAO();
        int result;
        
        User newUser = myConnection.login(activeUser,ConsoleInputs.askForEmail("Enter Email"),ConsoleInputs.askForPassword("Enter password"));
        
        if(newUser != null){
            activeUser = newUser;
            
            result = activeUser.getUserType();
        }else{
            result = -2;
        }
        
        switch(result){
            case 0:
                System.out.println("Login success");
                result = ownerMenu();
                switch(result){
                    case -1:
                        return true;
                    default:
                        return false;
                }
            case 1:
                System.out.println("Login success");
                result = adminMenu();
                switch(result){
                    case -1:
                        return true;
                    default:
                        return false;
                }
            case 2:
                System.out.println("Login success");
                result = customerMenu();
                switch(result){
                    case -1:
                        return true;
                    default:
                        return false;
                }
            case -2:
                System.out.println("Problem to connect to the databse");
            default:
                System.out.println("Incorrect username or password");
        }
         return false;
    }
    /**
     * Delete the current active user
     */
    private static void logout(){
        activeUser = new User();
    }
    /**
     *  Retrieve the details of a specific loan.
     */
    private static void seeSingleLoanDetails(){
        int intResult = 0;
        int bookId;
        int userId;
        if(activeUser.getUserType() == 1 || activeUser.getUserType() == 0){
            // See loan For admins and owner            
            //Checks for the loan in active loans
            bookId = ConsoleInputs.askForPositiveInt("Enter the book id");
            userId = ConsoleInputs.askForPositiveInt("Enter the id of the customer");
            
        }else{
            // See loan details for customers
            bookId = ConsoleInputs.askForPositiveInt("Enter the book id");
            userId = activeUser.getUserId();
        }
        
        ActiveLoanDAO activeLoans = new ActiveLoanDAO();
        Loan result = activeLoans.getActiveLoan(activeUser,userId,bookId);
        
        if(result != null){
            if(result.getLoan_id() != 0 ){
                System.out.println(result);
                intResult = 1;
            }
            // if the loan is not in active loans, it looks for it in loan records
            LoanRecordDAO loanRecords = new LoanRecordDAO();
            result = loanRecords.getSingleLoanRecord(activeUser, userId, bookId);
            if(result != null){
                if(result.getLoan_id() != 0 ){
                    intResult = 1;
                }
                intResult = 0;
            }
        }        
        intResult = 2;
        
        switch(intResult){
            case 0:
                System.out.println("The loan doesn't exist");
                break;
            case 1:
                System.out.println(result);
                break;
            case 2:
                System.out.println("Problems with the database, try again later please");
                break;
            case 3:
                System.out.println("You don't have privilages to do this");
                break;
            default:
                System.out.println("Something weird happened");
        }
    }
    /**
     * Remove a Book from the library.
     */
    private static void removeBook() {
        BookDAO books = new BookDAO();
        int result = books.removeBook(activeUser, ConsoleInputs.askForPositiveInt("Enter the book id of the book you want to delete"));
        
         switch(result){
            case 0:
                System.out.println("the Book couldn't be deleted");
                break;
            case 1:
                System.out.println("Book deleted");
                break;
            case 2:
                System.out.println("The book doesn't exist");
                break;
            case 3:
                System.out.println("The book has active loans. It cannot be deleted");
                break;
            case 4:
                System.out.println("You don't have privileges to delete the book");
                break;
            default:
                System.out.println("Unkown error acourred");
        }
    }
    
    /**
     * This method is used to retrieve inputs needed from user to update the number of copies a book has in the library, but undergoes 
     * validations and check points.
     * 
     */
    private static void updateBookQuantity(){
        BookDAO connectBookDAO = new BookDAO();
        int result = 0;
        
        System.out.println("Update Book's Quantity ");
        result = connectBookDAO.updateCopiesOfBook(activeUser, ConsoleInputs.askForInt("Book ID: "), 
                ConsoleInputs.askForPositiveInt("New Quantity: "));
        switch(result){
            case 0:
                System.out.println("Updating book's quantity wasn't successful");
                break;
            case 1:
                System.out.println("Book quantity has been succesfully updated!");
                break;
            case 2:
                System.out.println("Book ID couldn't be found.");
                break;
            case 3:
                System.out.println("Wait until some copies of the book are returned before updating the quantity");
                break;
            case 4:
                 System.out.println("You don't have privilages to do this");
                break;
        }
    }

    /**
     * This method is used to retrieve inputs needed from user to edit the book details. 
     * @return an integer depending on the result
     */
    private static void editBookDetails(){
        BookDAO connectBookDAO = new BookDAO();
        Book book = new Book();
        HashMap<Integer,String> updatedDetails = new HashMap<Integer,String>();
        int result;
        int menu;
        
        System.out.println("Retrieve Book by Book ID");
        book = connectBookDAO.searchBookByID(activeUser, ConsoleInputs.askForInt("Book ID: "));
        
        do
        {
            System.out.println("Edit Book Details");
            System.out.println("1) Title: " +book.getTitle());
            System.out.println("2) Author: " +book.getAuthor());
            System.out.println("3) Description: " +book.getDescription());
            System.out.println("4) Release Date: " +book.getRelease_date());
            System.out.println("5) Quantity: " +book.getQuantity());
            System.out.println("6) Loan time: " +book.getLoan_time());
            System.out.println("7) Overdue fee: " +book.getOverdue_fee());
            System.out.println("8) Exit/Done");
            
            menu = ConsoleInputs.askForValidMenuOption("Input option to edit: ", 8);
            
            switch(menu){
                case 1: updatedDetails.put(menu, ConsoleInputs.askForString("Update Title of the Book: "));
                    break;
                case 2: updatedDetails.put(menu, ConsoleInputs.askForString("Update Author of the Book: "));
                    break;
                case 3: updatedDetails.put(menu, ConsoleInputs.askForString("Update Description of Book: "));
                    break;
                case 4: updatedDetails.put(menu, ConsoleInputs.askForString("Update Release Date of Book: "));
                    break;
                case 5: updatedDetails.put(menu, ConsoleInputs.askForString("Update Quantity of Book: "));
                    break;
                case 6: updatedDetails.put(menu, ConsoleInputs.askForString("Update Loan Time of Book: "));
                    break;
                case 7: updatedDetails.put(menu, ConsoleInputs.askForString("Update Overdue fee of Book: "));
                    break;
                case 8: System.out.println("Processing new details.");
                    break;
            }
        }while(menu != 8);
        
        result = connectBookDAO.editBookDetails(activeUser, book, updatedDetails);
        
        switch(result){
            case -2: System.out.println("Matching details to an existing book, please check your details");
                break;
            case -1: System.out.println("Problem with database, please try again.");
                break;
            case 0: System.out.println("Couldn't update book details");
                break;
            case 1: System.out.println("Book updated successfully");
                break;
            default: System.out.println("User doesn't have privileges");
        }
    }

    /**
     * This method is used to retrieve user inputs to this specific request, and displays DAO response into user friendly messages.
     */
    private static void returnBook() {
        ActiveLoanDAO activeLoans = new ActiveLoanDAO();
        switch(activeUser.getUserType()){
            case 0:
            case 1:
                System.out.println("1) Return one of your books");
                System.out.println("2) Return a customer's book");
                System.out.println("3) Back to menu");
                int option = ConsoleInputs.askForValidMenuOption("Select an option", 3); 
                switch(option){
                    case 3:
                        
                        break;
                    default:
                        double result = 0;
                        switch(option){
                            case 1:
                                result = activeLoans.returnActiveLoan(activeUser, activeUser.getUserId(), ConsoleInputs.askForPositiveInt("Enter the book id"));
                                break;
                            case 2:
                                result = activeLoans.returnActiveLoan(activeUser, ConsoleInputs.askForPositiveInt("Enter the customer id"), ConsoleInputs.askForPositiveInt("Enter the book id"));
                        }
                        
                        if(result > 0){
                            
                            Locale currentLocale = new Locale("en","IE");
                            Currency currentCurrency = Currency.getInstance(currentLocale);
                            NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(currentLocale);

                            System.out.println("The book has being returned late. You have an overdue fee of "+currencyFormatter.format(result));
                            
                        }else{
                            switch((int)result){
                                case 0:
                                    System.out.println("We couldn't find that book");
                                    break;
                                case -1:
                                    System.out.println("Book return succesfully");
                                    break;
                                case -2:
                                    System.out.println("Problem with the database, try again");
                                    break;
                                default:
                                    System.out.println("You don't have privileges to return a book for someone else");
                                    break;
                                
                            }
                        }
                }
        }
        
    }

    /**
     * Display the console to remove an user and the result.
     */
    private static void removeUser() {
        UserDAO users = new UserDAO();
        int result = users.removeUser(activeUser, ConsoleInputs.askForPositiveInt("Enter the id of the user you want to delete"));
        switch(result){
            case 0:
                System.out.println("The user wasn't deleted");
                break;
            case 1:
                System.out.println("User deleted succesfully");
                break;
            case 2:
                System.out.println("Problem with the database, try again");
                break;
            default:
                System.out.println("You don't have privileges to delete this user");
                break;

        }
    }
    /**
     * Display the console see all books in the library and the result.
     */
    private static void seeAllBooks(){
        ArrayList<Book> list = new ArrayList<Book>();
        BookDAO connectBookDAO = new BookDAO();
        
        System.out.println("See all books in the library");
        list = connectBookDAO.seeAllBooks(activeUser);
        
        if(list.isEmpty()){
            System.out.println("Library is empty.");
        }
        else {
            for(Book title: list){
                System.out.println(title);
            }
        }
    }
    /**
     * Display the console see all books in the library and the result for customers.
     * @param activeUser User that is logged in to check that it is a customer;
     */
    private static void seeAllBooksCustomer(User activeUser){
        ArrayList<Book> list = new ArrayList<Book>();
        BookDAO connectBookDAO = new BookDAO();
        
        System.out.println("See all books in the library");
        list = connectBookDAO.seeAllBooksByCustomer();
        
        if(list.isEmpty()){
            System.out.println("Library is empty.");
        }
        else {
            for(Book title: list){
                System.out.println(title);
            }
        }
    }
    /**
     * Display the console to see all the active Loans for administrators and the result.
     */
    private static void seeAllActiveLoans(){
        ArrayList<Loan> list = new ArrayList<Loan>();
        ActiveLoanDAO connection = new ActiveLoanDAO();
        
        System.out.println("See all active loans");
        list = connection.seeAllActiveLoans(activeUser);
        
        for(Loan loan: list){
            System.out.println(loan);
        }
    }
    /**
     * Display the console to register a nu customer and the result.
     */
    private static void register(){
        User newUser = new User();

        newUser.setName(ConsoleInputs.askForStringLargeThan("Enter admin name", 2));
        newUser.setSurname(ConsoleInputs.askForStringLargeThan("Enter admin surname", 2));
        newUser.setAddress1(ConsoleInputs.askForStringLargeThan("Enter address line 1", 2));
        newUser.setAddress2(ConsoleInputs.askForStringLargeThan("Enter address line 2", 2));
        newUser.setTown(ConsoleInputs.askForStringLargeThan("Enter city/town", 2));
        newUser.setCounty(ConsoleInputs.askForStringLargeThan("Enter county", 2));
        newUser.setPhone(ConsoleInputs.askForPhone("Enter contact number"));
        newUser.setEmail(ConsoleInputs.askForEmail("Enter email"));
        newUser.setPassword(ConsoleInputs.askForPassword("The password has to have:"+'\n'+"-Uppercase"+'\n'+"-Lowercase"+'\n'+"A number"+'\n'+"A special character @ # $ % "));

        UserDAO myConnection = new UserDAO();

        switch(myConnection.addNewCustomer(newUser)){
            case 0:
                System.out.println("User couldn't be created");
                break;
            case 1:
                System.out.println("User created");
                break;
            case 2:
                System.out.println("Problems with the database");
                break;
        }
    }
    /**
     * Display the console to see all the loans of the active user( for customers).
     */
    private static void seeAllMyLoans() {
        ArrayList<Loan> loans;
        ActiveLoanDAO activeLoans = new ActiveLoanDAO();
        loans = activeLoans.seeAllActiveLoans(activeUser);
        
        if(loans != null && !loans.isEmpty()){
            System.out.println(loans);
        }
        System.out.println("You don't have any active loans");
        LoanRecordDAO loanRecords = new LoanRecordDAO();
        System.out.println(loanRecords.getAllLoanRecordsByCustomer(activeUser, activeUser.getUserId()));
        
        if(loans != null && !loans.isEmpty()){
            System.out.println(loans);
        }
        System.out.println("You don't have loan records");
        
    }

    private static void seeAllLoanByCustomer() {
        ArrayList<Loan> loans;
        ActiveLoanDAO activeLoans = new ActiveLoanDAO();
        int userId = ConsoleInputs.askForPositiveInt("Enter the user id");
        loans = activeLoans.seeAllActiveLoansByCustomer(activeUser, userId);
        
        if(loans != null && !loans.isEmpty()){
            System.out.println(loans);
        }
        System.out.println("You don't have any active loans");
        LoanRecordDAO loanRecords = new LoanRecordDAO();
        loans = loanRecords.getAllLoanRecordsByCustomer(activeUser, userId);
        
        if(loans != null && !loans.isEmpty()){
            System.out.println(loans);
        }
        System.out.println("You don't have loan records");
    }

    private static void seeAllMyCurrentLoans() {
        ArrayList<Loan> loans;
        ActiveLoanDAO activeLoans = new ActiveLoanDAO();
        loans = activeLoans.seeAllActiveLoans(activeUser);
        
        if(loans != null && !loans.isEmpty()){
            System.out.println(loans);
        }
        System.out.println("You don't have any active loans");
    }
}

