-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 31, 2017 at 12:51 AM
-- Server version: 10.1.24-MariaDB
-- PHP Version: 7.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `library_system`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `addAdminUser` (IN `firstName` VARCHAR(10), IN `lastName` VARCHAR(20), IN `addres1` VARCHAR(50), IN `addres2` VARCHAR(50), IN `newTown` VARCHAR(20), IN `newCounty` VARCHAR(50), IN `phone` VARCHAR(20), IN `newEmail` VARCHAR(50), IN `newPassword` VARCHAR(24))  BEGIN
INSERT INTO user(user_type,first_name,last_name,address_line_1,address_line_2,town,county,phone_number,email,password) VALUES(1, firstName, lastName, addres1, addres2, newTown, newCounty, phone, newEmail, newPassword);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `addBook` (IN `title` VARCHAR(50), IN `description` VARCHAR(200), IN `author` VARCHAR(50), IN `release_date` DATE, IN `quantity` INT(2), IN `loan_time` INT(2), IN `overdue_fee` FLOAT)  BEGIN
INSERT INTO book(title, description, author, release_date, quantity, loan_time, overdue_fee)
VALUES(title, description, author, release_date, quantity, loan_time, overdue_fee);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `addNewCustomer` (IN `firstName` VARCHAR(10), IN `lastName` VARCHAR(20), IN `addres1` VARCHAR(50), IN `addres2` VARCHAR(50), IN `newTown` VARCHAR(20), IN `newCounty` VARCHAR(50), IN `phone` VARCHAR(20), IN `newEmail` VARCHAR(50), IN `newPassword` VARCHAR(24))  BEGIN
INSERT INTO user(user_type,first_name,last_name,address_line_1,address_line_2,town,county,phone_number,email,password) VALUES(2, firstName, lastName, addres1, addres2, newTown, newCounty, phone, newEmail, newPassword);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `addNewLoanRecord` (IN `userId` INT(4), IN `bookId` INT(4), IN `startDate` DATE, IN `endDate` DATE, IN `returnDate` DATE)  BEGIN
INSERT INTO active_loan(user_id, book_id, date_start, date_end, return_date) 
VALUES (userId, bookId, startDate, endDate, returnDate);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `countActiveLoanByBookID` (IN `bookId` INT(4))  BEGIN
SELECT COUNT(*) AS "numLoan" FROM active_loan WHERE bookId = book_id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `countActiveLoanByUserID` (IN `userId` INT(4))  BEGIN
SELECT COUNT(*) AS "numLoan" FROM active_loan WHERE user_id = userId;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `createLoan` (IN `userId` INT(4), IN `bookId` INT(4), IN `dateStart` DATE, IN `dateEnd` DATE)  BEGIN
INSERT INTO active_loan(user_id, book_id, date_start, date_end) 
VALUES (userId, bookId, dateStart, dateEnd);
COMMIT;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `editBookDetails` (IN `book_id` INT(4), IN `title` VARCHAR(50), IN `description` VARCHAR(200), IN `author` VARCHAR(50), IN `release_date` DATE, IN `quantity` INT(2), IN `loan_time` INT(2), IN `overdue_fee` FLOAT)  BEGIN
UPDATE book
SET 
title = title,
description = description,
author = author,
release_date = release_date,
quantity = quantity,
loan_time = loan_time,
overdue_fee = overdue_fee
WHERE 
book_id = book_id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getActiveLoan` (IN `userId` INT(4), IN `bookId` INT(4))  BEGIN
SELECT * FROM active_loan WHERE user_id = userId AND bookId = book_id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getAllLoanRecords` (IN `userId` INT(4))  BEGIN
SELECT * FROM loan_record_history WHERE use_id = userId;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getSingleLoanRecord` (IN `userId` INT(4), IN `bookId` INT(4))  BEGIN
SELECT * FROM loan_record_history WHERE use_id = userId AND book_id = bookId;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getUserById` (IN `userId` INT(4))  BEGIN
SELECT * FROM user WHERE user_id = userId;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `login` (IN `inputEmail` VARCHAR(50), IN `inputPass` VARCHAR(24))  BEGIN
SELECT first_name,last_name,user_id,user_type FROM user WHERE  email = inputEmail AND user_pass = inputPass;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `removeBook` (IN `book_id` INT(4))  BEGIN 
DELETE FROM book
WHERE book_id = book_id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `removeUser` (IN `userId` INT(4))  BEGIN
UPDATE user SET user_pass = ‘DELETED’ WHERE user_id = userId;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `searchBookAdmin` (IN `book_id` INT(4))  BEGIN 
SELECT * FROM book
WHERE book_id = book_id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `searchBookById` (IN `book_id` INT(4))  BEGIN 
SELECT * FROM book
WHERE book_id = book_id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `searchBookByTitle` (IN `title` VARCHAR(50))  BEGIN
SELECT * FROM book
WHERE title = title;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `seeAllBooks` ()  BEGIN
SELECT * FROM book;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `seeAllLoans` ()  BEGIN
SELECT * FROM loan;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `seeAllLoansByCustomer` (IN `userId` INT(4))  BEGIN
SELECT * FROM active_loan WHERE use_id = userId;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `active_loan`
--

CREATE TABLE `active_loan` (
  `loan_id` int(4) NOT NULL,
  `user_id` int(4) NOT NULL,
  `book_id` int(4) NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE `book` (
  `book_id` int(4) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL,
  `author` varchar(50) NOT NULL,
  `release_date` date NOT NULL,
  `quantity` int(2) NOT NULL,
  `loan_time` int(2) NOT NULL,
  `overdue_fee` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `book_category`
--

CREATE TABLE `book_category` (
  `book_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` int(4) NOT NULL,
  `category_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `loan_record_history`
--

CREATE TABLE `loan_record_history` (
  `loan_record_id` int(4) NOT NULL,
  `user_id` int(50) NOT NULL,
  `book_id` int(4) NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `return_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(4) NOT NULL,
  `user_type` varchar(10) NOT NULL DEFAULT '2',
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `address_line_1` varchar(50) NOT NULL,
  `address_line_2` varchar(50) DEFAULT NULL,
  `town` varchar(20) NOT NULL,
  `county` varchar(50) NOT NULL,
  `phone_number` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `user_pass` varchar(24) NOT NULL,
  `outstanding_fee` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_type`, `first_name`, `last_name`, `address_line_1`, `address_line_2`, `town`, `county`, `phone_number`, `email`, `user_pass`, `outstanding_fee`) VALUES
(6, '0', 'Jorge', 'Vilaseco', 'DKIT', '', 'Dundalk', 'Louth', '0831234567', 'd00188294@student.dkit.ie', 'Jorge@owner1', NULL),
(7, '2', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `active_loan`
--
ALTER TABLE `active_loan`
  ADD PRIMARY KEY (`loan_id`);

--
-- Indexes for table `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`book_id`);

--
-- Indexes for table `book_category`
--
ALTER TABLE `book_category`
  ADD PRIMARY KEY (`book_id`,`category_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `active_loan`
--
ALTER TABLE `active_loan`
  MODIFY `loan_id` int(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `book`
--
ALTER TABLE `book`
  MODIFY `book_id` int(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
