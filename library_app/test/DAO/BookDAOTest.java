/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DTO.Book;
import DTO.User;
import java.util.ArrayList;
import java.util.HashMap;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author User
 */
public class BookDAOTest {
    
    public BookDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of addBook method, of class BookDAO.
     */
    @Test
    public void testAddBook() {
        System.out.println("addBook");
        User newActiveUser = new User();
        Book newBookDetails = new Book(); 
        
        newBookDetails.setTitle("Hello World");
        newBookDetails.setDescription("DEaling withdepression");
        newBookDetails.setAuthor("James Munroe");
        newBookDetails.setRelease_date("10/10/2017");
        newBookDetails.setQuantity(8);
        newBookDetails.setLoan_time(7);
        newBookDetails.setOverdue_fee(0.00);
        
        
        BookDAO instance = new BookDAO();
        int expResult = 0;
        int result = instance.addBook(newActiveUser, newBookDetails);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of searchBookByID method, of class BookDAO.
     */
    @Test
    public void testSearchBookByID() {
        System.out.println("searchBookByID");
        User newActiveUser = new User();
        int book_id = 1;
        BookDAO instance = new BookDAO();
        Book expResult = null;
        Book result = instance.searchBookByID(newActiveUser, book_id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of loanABook method, of class BookDAO.
     */
    @Test
    public void testLoanABook() {
        System.out.println("loanABook");
        User newActiveUser = new User();
        int book_id = 2;
        BookDAO instance = new BookDAO();
        double expResult = 0.0;
        double result = instance.loanABook(newActiveUser, book_id);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of removeBook method, of class BookDAO.
     */
    @Test
    public void testRemoveBook() {
        System.out.println("removeBook");
        User newActiveUser = new User();
        int book_id = 2;
        BookDAO instance = new BookDAO();
        int expResult = 3;
        int result = instance.removeBook(newActiveUser, book_id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of updateCopiesOfBook method, of class BookDAO.
     */
    @Test
    public void testUpdateCopiesOfBook1() {
        System.out.println("updateCopiesOfBook");
        User newActiveUser = new User();
        int book_id = 1;
        int newQuantity = 7;
        BookDAO instance = new BookDAO();
        int expResult = 3;
        int result = instance.updateCopiesOfBook(newActiveUser, book_id, newQuantity);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
     
    @Test
    public void testUpdateCopiesOfBook2() {
        System.out.println("updateCopiesOfBook");
        User newActiveUser = new User();
        int book_id = 1;
        int newQuantity = 12;
        BookDAO instance = new BookDAO();
        int expResult = 1;
        int result = instance.updateCopiesOfBook(newActiveUser, book_id, newQuantity);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }


    /**
     * Test of searchBookByTitle method, of class BookDAO.
     */
    @Test
    public void testSearchBookByTitle() {
        System.out.println("searchBookByTitle");
        User newActiveUser = new User();
        String title = "Money";
        BookDAO instance = new BookDAO();
        Book expResult = new Book();
        expResult.setBook_id(1);
        expResult.setTitle("Money");
        expResult.setAuthor("Mr Moneybags");
        expResult.
        expResult.setRelease_date();
        expResult.getQuantity();
        expResult.getLoan_time();
        expResult.getOverdue_fee();
        
        Book result = instance.searchBookByTitle(newActiveUser, title);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of editBookDetails method, of class BookDAO.
     */
    @Test
    public void testEditBookDetails() {
        System.out.println("editBookDetails");
        User activeUser = null;
        Book book = null;
        HashMap<Integer, String> updatedDetails = null;
        BookDAO instance = new BookDAO();
        int expResult = 0;
        int result = instance.editBookDetails(activeUser, book, updatedDetails);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getOverdueFee method, of class BookDAO.
     */
    @Test
    public void testGetOverdueFee() {
        System.out.println("getOverdueFee");
        User activeUser = null;
        int book_id = 0;
        BookDAO instance = new BookDAO();
        double expResult = 0.0;
        double result = instance.getOverdueFee(activeUser, book_id);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of seeAllBooks method, of class BookDAO.
     */
    @Test
    public void testSeeAllBooks() {
        System.out.println("seeAllBooks");
        User activeUser = null;
        BookDAO instance = new BookDAO();
        ArrayList<Book> expResult = null;
        ArrayList<Book> result = instance.seeAllBooks(activeUser);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of seeAllBooksByCustomer method, of class BookDAO.
     */
    @Test
    public void testSeeAllBooksByCustomer() {
        System.out.println("seeAllBooksByCustomer");
        BookDAO instance = new BookDAO();
        ArrayList<Book> expResult = null;
        ArrayList<Book> result = instance.seeAllBooksByCustomer();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
