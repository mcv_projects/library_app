/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DTO.Loan;
import DTO.User;
import java.time.LocalDate;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author User
 */
public class ActiveLoanDAOTest {
    
    public ActiveLoanDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of createLoan method, of class ActiveLoanDAO.
     * Testing userId 2 loaning bookId 1. It is successful
     */
    @Test
    public void testCreateLoan1()throws Exception {
        System.out.println("createLoan");
        Loan newLoan = new Loan();
        
        newLoan.getCust().setUserId(2);
        newLoan.getBook().setBook_id(1);
        newLoan.setDate_start(LocalDate.of(2017, 10, 31));
        newLoan.setDate_end(LocalDate.of(2017, 11, 1));
        
        ActiveLoanDAO instance = new ActiveLoanDAO();
        int expResult = 0;
        int result = instance.createLoan(newLoan);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    /**
     * Test of getActiveLoan method, of class ActiveLoanDAO.
     */
    @Test
    public void testGetActiveLoan1() {
        System.out.println("getActiveLoan has no privilages");
        User newActiveUser = new User();
        newActiveUser.setUserType(2);
        int book_id = 1;
        int user_id = 1; // id of user registered user to that loan
        ActiveLoanDAO instance = new ActiveLoanDAO();
        Loan expResult = null;
        Loan result = instance.getActiveLoan(newActiveUser, book_id, user_id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    

    @Test
    public void testGetActiveLoan2() {
        System.out.println("getActiveLoan - loan wasn`t found");
        User newActiveUser = new User();
        newActiveUser.setUserType(0);
        int book_id = 999;
        int user_id = 1;
        ActiveLoanDAO instance = new ActiveLoanDAO();
        Loan expResult = new Loan();
        Loan result = instance.getActiveLoan(newActiveUser, book_id, user_id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    @Test
    public void testGetActiveLoan3() {
        System.out.println("getActiveLoan - loan was found");
        User newActiveUser = new User();
        newActiveUser.setUserType(0);
        int book_id = 1;// book id i know is in the database
        int user_id = 2; // user id i know is in the database
        ActiveLoanDAO instance = new ActiveLoanDAO();
        Loan expResult = new Loan();
        // set up what we expect to get
        expResult.getCust().setUserId(2);
        expResult.getBook().setBook_id(1);
        expResult.setDate_start(LocalDate.of(2017, 10, 31));
        expResult.setDate_end(LocalDate.of(2017, 11, 1));
        expResult.setLoan_id(2);
 
        Loan result = instance.getActiveLoan(newActiveUser, book_id, user_id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    /**
     * Test of countActiveLoanByBookID method, of class ActiveLoanDAO.
     * 
     */
    @Test
    public void testCountActiveLoanByBookID1() {
        System.out.println("countActiveLoanByBookID");    
        // This will -1 because there are no active loans with book id 1
        int book_id = 1;
        ActiveLoanDAO instance = new ActiveLoanDAO();
        int expResult = -1;
        int result = instance.countActiveLoanByBookID(book_id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
     @Test
    public void testCountActiveLoanByBookID2() {
        System.out.println("countActiveLoanByBookID");    
        // This will return -1 
        int book_id = 0;
        ActiveLoanDAO instance = new ActiveLoanDAO();
        int expResult = -1;
        int result = instance.countActiveLoanByBookID(book_id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
 
    /**
     * Test of returnActiveLoan method, of class ActiveLoanDAO.
     */
    @Test
    public void testReturnActiveLoan1() {
        System.out.println("returnActiveLoan");
        User newActiveUser = new User();
        newActiveUser.setUserType(1);
        int user_id = 1;
        int book_id = 1;
        ActiveLoanDAO instance = new ActiveLoanDAO();
        double expResult = -2.0;
        double result = instance.returnActiveLoan(newActiveUser, user_id, book_id);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    /**
     * Test of returnActiveLoan method, of class ActiveLoanDAO.
     */
    @Test
    public void testReturnActiveLoan2() {
        System.out.println("returnActiveLoan");
        User newActiveUser = new User();
        newActiveUser.setUserType(1);
        int user_id = 1;
        int book_id = 1;
        ActiveLoanDAO instance = new ActiveLoanDAO();
        Loan expResult = new Loan();
        // set up what we expect to get
        expResult.getCust().getUserId();
        expResult.getBook().getBook_id();
        expResult.getDate_start();
        expResult.getDate_end();
        
        //double expResult = -2.0;
        double result = instance.returnActiveLoan(newActiveUser, user_id, book_id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
   }
    
}
