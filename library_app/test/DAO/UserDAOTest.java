/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DTO.User;
import java.time.LocalDate;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jorgevilasecojurado
 */
public class UserDAOTest {
    
    public UserDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of addAdmin method, of class UserDAO.
     */
    @Test
    public void testAddAdmin() throws Exception {
        System.out.println("addAdmin Success");
        User newUser = new User();
        User activeUser = new User();
        
        newUser.setAddress1("Test");
        newUser.setAddress2("Test");
        newUser.setCounty("Test");
        newUser.setEmail("Test");
        newUser.setName("Test");
        newUser.setPassword("Test");
        newUser.setPhone("Test");
        newUser.setSurname("Test");
        newUser.setTown("Test");
        newUser.setUserType(1);
        
        
        activeUser.setUserType(0);
        
        UserDAO instance = new UserDAO();
        User expResult = new User();
        
        
        User result = instance.addAdmin(newUser, activeUser);
        assertEquals(expResult, result);
    }
    /**
     * Test of addAdmin method, of class UserDAO.
     */
    @Test
    public void testAddAdmin1() throws Exception {
        System.out.println("addAdmin Access denied");
        User newUser = new User();
        User activeUser = new User();
        
        newUser.setAddress1("Test");
        newUser.setAddress2("Test");
        newUser.setCounty("Test");
        newUser.setEmail("Test");
        newUser.setName("Test");
        newUser.setPassword("Test");
        newUser.setPhone("Test");
        newUser.setSurname("Test");
        newUser.setTown("Test");
        newUser.setUserType(1);
        
        activeUser.setUserType(1);
        
        UserDAO instance = new UserDAO();
        String expResult = "ACCESS_DENIED";
        String result = instance.addAdmin(newUser, activeUser);
        assertEquals(expResult, result);
    }
    /**
     * Test of addAdmin method, of class UserDAO.
     */
    @Test
    public void testAddAdmin2() throws Exception {
        System.out.println("addAdmin Fail");
        User newUser = new User();
        User activeUser = new User();
        
        newUser.setAddress1("Test");
        newUser.setAddress2("Test");
        newUser.setCounty("Test");
        newUser.setPassword("Test");
        newUser.setPhone("Test");
        newUser.setSurname("Test");
        newUser.setTown("Test");
        newUser.setUserType(1);
        
        activeUser.setUserType(0);
        
        UserDAO instance = new UserDAO();
        String expResult = "FAILED";
        String result = instance.addAdmin(newUser, activeUser);
        assertEquals(expResult, result);
    }
}
