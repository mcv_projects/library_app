
package DTO;

import java.util.Objects;

/**
 *
 * @author jorgevilasecojurado
 */
public class User {
    private int userId = -1;
    private int userType = -1;
    private String name;
    private String surname;
    private String address1;
    private String address2;
    private String town;
    private String county;
    private String phone;
    private String email;
    private String password;
    private double overDueFee;

    public User() {
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public double getOverDueFee() {
        return userId;
    }

    public void setOverDueFee(double overDueFee) {
        this.overDueFee = overDueFee;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + this.userId;
        hash = 37 * hash + Objects.hashCode(this.email);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (this.userId != other.userId) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "\nRegistered User" + 
                "\nUser ID: " + userId + 
                "\nUser type: " + userType + 
                "\nName: " + name + 
                "\nSurename: " + surname + 
                "\nAddress Line 1: " + address1 + 
                "\nAddress Line 2: " + address2 + 
                "\nTown: " + town + 
                "\nCounty: " + county + 
                "\nPhone number: " + phone + 
                "\nEmail address: " + email + 
                "\nPassword: " + password;
    }
    
    
    
}
