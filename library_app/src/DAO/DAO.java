
package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author jorgevilasecojurado
 */
public class DAO {
    private String dbName = "library_system";
    /**
     * Constructor for live database
     */
    public DAO() {
    }
    /**
     * Constructor for testing
     * @param dbName String testing database name
     */
    public DAO(String dbName) {
        this.dbName = dbName;
    }
    /**
     *  Creates a Connection to a MySQL database 
     * 
     * @return a Connection Object
     * @throws ClassNotFoundException the drivers to connect to MySQL not found
     * @throws SQLException 1)The database is not online. <br>2)Access denied to the database 
     */
    public Connection connect() throws ClassNotFoundException, SQLException{
        
        
        // Create variables to hold database details
        String driver = "com.mysql.jdbc.Driver";
        String url = "jdbc:mysql://127.0.0.1:3306/"+dbName;
        String username = "root";
        String password = "";
        // Create variables used to interact with database 
        // We need them created here so we can close them in the finally block
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        
        // Load the database driver
        Class.forName( driver ) ;

        // Get a connection to the database
        conn = DriverManager.getConnection(url, username, password) ;
        
        
        return conn;
    }
    /**
     *  Closes all the connections used 
     * @param conn Connection Object 
     * @param ps PreparedStatement Object
     * @param rs ResultSet Object
     */
    public void closeConnections(Connection conn, PreparedStatement ps, ResultSet rs){
        // Close the result set, statement and the connection
        if(rs!= null)
        {
            try
            {
                rs.close() ;
            } 
            catch (SQLException ex){
                System.out.println("Exception occurred when attempting to close ResultSet: " + ex.getMessage());
            }
        }
        if(ps != null)
        {
            try{
                ps.close() ;
            } 
            catch (SQLException ex){
                System.out.println("Exception occurred when attempting to close the PreparedStatement: " + ex.getMessage());
            }
        }
        if(conn != null){
            try{
                conn.close() ;
            } 
            catch (SQLException ex){
                System.out.println("Exception occurred when attempting to close the Connection: " + ex.getMessage());
            }
        }
    }
}
