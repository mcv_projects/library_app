/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DAOInterfaces.LoanRecordDAOInterface;
import DTO.Loan;
import DTO.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Date;
import java.util.ArrayList;

/**
 *
 * @author jorgevilasecojurado
 */
public class LoanRecordDAO extends DAO implements LoanRecordDAOInterface{
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private Connection conn = null;
    private int resultCount;
    /**
     * Retrieve a specific loan from the loan history
     * 
     * @param activeUser user that is asking for the loan
     * @param userId id of the user that made the loan
     * @param bookId id of the book that was loaned
     * @return a populated Loan if the loan exist, an empty Loan if it doesn't or null, if the user is not allow to see the loan
     */
    @Override
    public Loan getSingleLoanRecord(User activeUser, int userId, int bookId) {
        if(activeUser.getUserId() == userId || activeUser.getUserType() == 1 || activeUser.getUserType() == 0){
            
            try {
                conn = connect();

                ps = conn.prepareStatement("call getSingleLoanRecord(?,?)") ;

                ps.setInt(1, userId);
                ps.setInt(2, bookId);

                rs = ps.executeQuery();
                if(rs.next()){
                    Loan loanRecord = new Loan();
                    
                    loanRecord.setLoan_id(rs.getInt("loan_record_id"));
                    
                    UserDAO users = new UserDAO();
                    loanRecord.setCust(users.getUserById(rs.getInt("user_id")));
                    
                    BookDAO books = new BookDAO();
                    loanRecord.setBook(books.searchBookByID(activeUser,rs.getInt("book_id")));
                    
                    loanRecord.setDate_start(rs.getDate("date_start").toLocalDate());
                    loanRecord.setDate_end(rs.getDate("date_end").toLocalDate());
                    loanRecord.setReturn_date(rs.getDate("return_date").toLocalDate());
                    
                    super.closeConnections(conn, ps, rs);
                    
                    return loanRecord;
                }
                
                return new Loan();
                
            } catch (Exception ex) {
                System.out.println("Problem with the database "+ex.getMessage());
            }
        }
        return null;
    }

    @Override
    public int addNewLoanRecord(Loan newLoan, User activeUser) {
        if(activeUser.getUserId() == newLoan.getCust().getUserId() || activeUser.getUserType() == 1 || activeUser.getUserType() == 0){
            
            try {
                conn = connect();

                ps = conn.prepareStatement("call addNewLoanRecord(?,?,?,?,?)") ;

                ps.setInt(1, newLoan.getCust().getUserId());
                ps.setInt(2, newLoan.getBook().getBook_id());
                ps.setDate(3, Date.valueOf(newLoan.getDate_start()));
                ps.setDate(4, Date.valueOf(newLoan.getDate_end()));
                ps.setDate(5, Date.valueOf(newLoan.getReturn_date()));
                
                

                return ps.executeUpdate();
                
                
                
            } catch (Exception ex) {
                System.out.println("Problem with the database "+ex.getMessage());
                return 2;
            }
        } 
        return 3;
    }
    /**
     * Retrieve all the loan records of a customer.
     * It checks if the user is allow to see them.
     *  - Administrators and owners can see the records of every customer
     *  - Customer can see just their own records
     * 
     * @param activeUser User that wants to see the records
     * @param userId id of the user that made the loan
     * @return a populated ArrayList<Loan> if the user is allow to see them and there is some records for the user given,
     *          an empty ArrayList<Loan> if the user given doesn't have any record,
     *          Null if the user is not allow to see the records of the user given or an SQL error happened
     */
    @Override
    public ArrayList<Loan> getAllLoanRecordsByCustomer(User activeUser, int userId) {
        if(activeUser.getUserId() == userId || activeUser.getUserType() == 1 || activeUser.getUserType() == 0){
            try {
                conn = connect();

                ps = conn.prepareStatement("call getAllLoanRecords(?)") ;

                ps.setInt(1, userId);

                rs = ps.executeQuery();
                
                if(rs.next()){
                    
                    ArrayList<Loan> allLoanRecords = new ArrayList();
                    
                    while(rs.next()){
                        Loan loanRecord = new Loan();

                        loanRecord.setLoan_id(rs.getInt("loan_record_id"));

                        UserDAO users = new UserDAO();
                        loanRecord.setCust(users.getUserById(rs.getInt("user_id")));

                        BookDAO books = new BookDAO();
                        loanRecord.setBook(books.searchBookByID(activeUser,rs.getInt("book_id")));

                        loanRecord.setDate_start(rs.getDate("date_start").toLocalDate());
                        loanRecord.setDate_end(rs.getDate("date_end").toLocalDate());
                        loanRecord.setReturn_date(rs.getDate("return_date").toLocalDate());

                        super.closeConnections(conn, ps, rs);


                    }
                    return allLoanRecords;
                }
                return new ArrayList();
                
            } catch (Exception ex) {
                System.out.println("Problem with the database "+ex.getMessage());
            }
        }
        return null;
    }
    
}
