
package DAO;

import DAOInterfaces.UserDAOInterface;
import DTO.User;
import ValidationInterfaces.ConsoleInputs;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author jorgevilasecojurado
 */
public class UserDAO extends DAO implements UserDAOInterface{
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private Connection conn = null;
    private int resultCount;
    /**
     * Constructor for live database
     */
    public UserDAO(){
    }
    /**
     * Constructor for testing
     * @param dbName String testing database name
     */
    public UserDAO(String dbName) {
        super(dbName);
    }
    /**
     * Creates an Administrator user
     * 
     * @param newUser User object that has to be added to the database
     * @param activeUser Current User the is inserting the new user
     * @return 1 if the user was created, 0 if it wasn't, 2 if there was an SQL error and 3 if the user doesn't have the privileges to create this type of user
     */
    @Override
    public int addAdmin(User newUser, User activeUser){
        if(activeUser.getUserType() == 0){
            try {
                conn = connect();
                
                ps = conn.prepareStatement("call addAdminUser(?,?,?,?,?,?,?,?,?)") ;
                
                ps.setString(1, newUser.getName());
                ps.setString(2, newUser.getSurname());
                ps.setString(3, newUser.getAddress1());
                ps.setString(4, newUser.getAddress2());
                ps.setString(5, newUser.getTown());
                ps.setString(6, newUser.getCounty());
                ps.setString(7, newUser.getPhone());
                ps.setString(8, newUser.getEmail());
                ps.setString(9, newUser.getPassword());
                
                int result = ps.executeUpdate();
                
                super.closeConnections(conn, ps, rs);
               
                return result;
                
            } catch (Exception ex) {
                System.out.println("Problem with the database "+ex.getMessage());
                return 2;
            }
        }
        return 3;
    }
    /**
     * Add a customer user to the database.
     * 
     * @param newUser User object that has to be added to the database
     * @return 1 if the user was created, 0 if it wasn't and 2 if there was an SQL error
     */
    @Override
    public int addNewCustomer(User newUser){
        try {
            conn = connect();

            ps = conn.prepareStatement("call addNewCustomer(?,?,?,?,?,?,?,?,?)") ;

            ps.setString(1, newUser.getName());
            ps.setString(2, newUser.getSurname());
            ps.setString(3, newUser.getAddress1());
            ps.setString(4, newUser.getAddress2());
            ps.setString(5, newUser.getTown());
            ps.setString(6, newUser.getCounty());
            ps.setString(7, newUser.getPhone());
            ps.setString(8, newUser.getEmail());
            ps.setString(9, newUser.getPassword());

            int result = ps.executeUpdate();

            super.closeConnections(conn, ps, rs);

            return result;

        } catch (Exception ex) {
            System.out.println("Problem with the database "+ex.getMessage());
            return 2;
        }
    }
    /**
     * Retrieve the user information needed that matches the email and the password given
     * 
     * @param activeUser The current active user in the app 
     * @param email Sting for the email of the user that wants to login
     * @param password String for the password of that user
     * @return an User object if the login was successful, null otherwise
     */
    @Override
    public User login(User activeUser, String email, String password) {
        if(activeUser.getUserId() == -1){
            try {
                conn = connect();
                
                ps = conn.prepareStatement("call login(?,?)") ;
                
                ps.setString(1, email);
                ps.setString(2, password);
                
                rs = ps.executeQuery();
                if(rs.next()){
                    User newUser = new User();
                    
                    newUser.setName(rs.getString("first_name"));
                    newUser.setSurname(rs.getString("last_name"));
                    newUser.setUserId(rs.getInt("user_id"));
                    newUser.setUserType(rs.getInt("user_type"));
                    
                    super.closeConnections(conn, ps, rs);
                    return newUser;
                }
                super.closeConnections(conn, ps, rs);
                return null;
                
            } catch (Exception ex) {
                System.out.println("Problem with the database"+ex.getMessage());
            }
        }
        return null;
    }
    /**
     * Retrieve the outstanding fee of the active user.
     * 
     * @param activeUser user used to check its fee
     * @return return the outstanding fee of an user or -1 if there is an error 
     */
    @Override
    public double hasUserOverFee(User activeUser){
        try {
            conn = connect();

            ps = conn.prepareStatement("call getUserOverFee(?)") ;

            ps.setInt(1, activeUser.getUserId());

            rs = ps.executeQuery();
            if(rs.next()){
                return rs.getDouble("outstanding_fee");
            }
            super.closeConnections(conn, ps, rs);
            return 0;

        } catch (Exception ex) {
            System.out.println("Problem with the database "+ex.getMessage());
        }
        return -1;
    }
    /**
     * Search for an User by the given id
     * 
     * @param userId integer used to find the user
     * @return a populated User object if the user was found, an empty User if it wasn't and null if there was an error
     */
    @Override
    public User getUserById(int userId) {
        try {
            conn = connect();

            ps = conn.prepareStatement("call getUserById(?)") ;

            ps.setInt(1,userId);

            rs = ps.executeQuery();
            if(rs.next()){
                User theUser = new User();
                
                theUser.setUserId(rs.getInt("user_id"));
                theUser.setUserType(rs.getInt("user_type"));
                theUser.setName(rs.getString("first_name"));
                theUser.setSurname(rs.getString("last_name"));
                theUser.setAddress1(rs.getString("address_1"));
                theUser.setAddress2(rs.getString("address_2"));
                theUser.setTown(rs.getString("town"));
                theUser.setCounty(rs.getString("county"));
                theUser.setPhone(rs.getString("phone_number"));
                theUser.setEmail(rs.getString("email"));
                theUser.setOverDueFee(rs.getDouble("outstanding_fee"));
                
                return theUser;
                
            }
            super.closeConnections(conn, ps, rs);
            return new User();

        } catch (Exception ex) {
            System.out.println("Problem with the database "+ex.getMessage());
        }
        return null;
    }
    /**
     * Increase the fee of an user.
     * 
     * @param fee new value for the fee
     * @return 1 if it was successfully updated, 0 otherwise and 2 if there was an SQL error
     */
    @Override
    public int increaseUserOverFee(double fee) {
        try {
            conn = connect();

            ps = conn.prepareStatement("call increaseUserFee(?)") ;

            ps.setDouble(1, fee);

            int result = ps.executeUpdate();

            super.closeConnections(conn, ps, rs);

            return result;

        } catch (Exception ex) {
            System.out.println("Problem with the database "+ex.getMessage());
            return 2;
        }
    }
    /**
     * Remove an user from the database.
     * It checks the type of user so users cannot delete user they are not allow to
     *  - Administrators are allow to delete customers and other administrators
     *  - Owners are allow to delete any user that they want
     *  - Customers are allow to delete just their own account 
     * @param activeUser user that is trying to delete an user
     * @param userId of the user the want to delete
     * @return 
     *          - 1 if it was deleted successfully 
     *          - 0 if it wasn't 
     *          - 2 if there was a SQL error
     *          - 3 if the user doesn't have the privileges to delete the user
     */
    @Override
    public int removeUser(User activeUser, int userId) {
        if(activeUser.getUserId() == userId || activeUser.getUserType() == 1 || activeUser.getUserType() == 0 ){
            try {
                conn = connect();
                
                User userToDelete = getUserById(userId);
                
                if(userToDelete != null && userToDelete.getUserId() != -1 ){
                    if(activeUser.getUserType() >= userToDelete.getUserType() ){
                        ActiveLoanDAO activeLoans = new ActiveLoanDAO();
                        if(hasUserOverFee(userToDelete)>=0 && activeLoans.countActiveLoanByCustomerId(userId) == 0){
                            // We don't delete the user because we keep track of old loan and for that we need to keep the data of the user
                            // The method use to do it this is not the best, but no time to do it properly
                            ps = conn.prepareStatement("call removeUser(?)");

                            ps.setInt(1, userId);

                            int result = ps.executeUpdate();

                            super.closeConnections(conn, ps, rs);

                            return result;
                        }
                        switch(ConsoleInputs.askForValidMenuOption("Do you want to delete the user?"+'\n'+"1) Yes"+'\n'+"2) No",2)){
                            case 1:
                                // We don't delete the user because we keep track of old loan and for that we need to keep the data of the user
                                // The method use to do it this is not the best, but no time to do it properly
                                ps = conn.prepareStatement("call removeUser(?)");

                                ps.setInt(1, userId);

                                int result = ps.executeUpdate();

                                super.closeConnections(conn, ps, rs);

                                return result;
                            case 2:
                                return 0;
                        }
                    }
                    return 3;
                }
            } catch (Exception ex) {
                System.out.println("Problem with the database "+ex.getMessage());
                return 2;
            }
        }
        return 3;
    }

}
