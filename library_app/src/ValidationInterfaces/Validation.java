package ValidationInterfaces;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;



/**
 *
 * @author jorgevilasecojurado
 */
public interface Validation {
     
    /**
     * Check if the string given is empty
     * 
     * @param input String to check
     * @return true if the string is empty, false otherwise
     */
    static public boolean isEmptyString(String input){
        return input.length() < 1;
    }
    /**
     * Check if the size of a String is bigger or equal to a given number
     * 
     * @param input String to be checked 
     * @param minSize minimum size to check the input
     * @return true is the input is equal or bigger than the minimum size, false otherwise.
     */
    static public boolean isStringLargerThan(String input, int minSize){
        return input.length() >= minSize;
    }
    /**
     * Check if the given number is bigger or equal than the minimum size given
     * 
     * @param input Number to be checked
     * @param minSize Minimum size for the check
     * @return true if the input is bigger or equal to minSize, false otherwise
     */
    static public boolean isIntBiggerThan(int input, int minSize){
        return input >= minSize;
    }
    /**
     * Checks if the structure of the given String matches an email structure
     * 
     * @param input String to check
     * @return true if the String has a valid email structure, false otherwise
     */
    static public boolean isValidEmail(String input){
        String emailPattern = "^([_a-zA-Z0-9-]+(\\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*(\\.[a-zA-Z]{1,6}))?$";
        return Pattern.matches(emailPattern, input);
    }
    /**
     * Check if the input has a valid irish phone structure.
     * It checks the size, 10 digits, and that they are all numbers
     * 
     * @param input String to check
     * @return true if the structure is correct, false otherwise
     */
    static public boolean isValidPhone(String input){
        String phonePattern = "\\b[0-9]{10}\\b";
        return Pattern.matches(phonePattern, input);
    }
    /**
     * Checks if the option given is between 1 and the number given
     * 
     * @param input 
     * @param numOfOption
     * @return true if the option is valid, false otherwise
     */
    static public boolean isValidMenuOption(int input, int numOfOption){
        return input <= numOfOption && input > 0;
    }
    /**
     * Checks if the number given is positive
     * 
     * @param input integer given
     * @return true if the number is positive, false otherwise
     */
    static public boolean isPositive(int input){
        return input >= 0;
    }
    /**
     * Checks if the number given is positive
     * 
     * @param input double given
     * @return true if the number is positive, false otherwise
     */
    static public boolean isPositive(double input){
        return input >= 0;
    }
    /**
     * Checks if the String given fits the criteria to be a valid password.
     * The criteria is:
     *      - At least a Uppercase
     *      - At least a Lowercase
     *      - At least a number
     *      - At least a special character @ # $ % !
     * 
     * @param input String to be checked
     * @return true if the string meets the criteria, false otherwise
     */
    static public boolean isPasswordValid(String input){
        String passwordPattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%!])(?=\\S+$).{8,}$";
        return Pattern.matches(passwordPattern, input);
    }
    /**
     * Checks if the format of the String given fits a date format
     * @param input String to check
     * @return true if the input has a date format, false otherwise
     */
    static public boolean isDateFormatValid(String input){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/mm/yyyy");
	dateFormat.setLenient(false); ;
        try {
            Date date = dateFormat.parse(input);
            return true;

        } catch (ParseException e) {
            return false;
        }
    }
}
