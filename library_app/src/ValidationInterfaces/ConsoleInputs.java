
package ValidationInterfaces;

import java.util.Scanner;

/**
 *
 * @author jorgevilasecojurado
 */
public interface ConsoleInputs{
    static Scanner read = new Scanner(System.in);
    /**
     * Ask the user to input a String until it is not empty
     * 
     * @param title String to explain the user what needs to be inputed
     * @return a non empty String
     */
    static public String askForString(String title){
        String input;
        while(true){
            System.out.println(title);
            input = read.nextLine();
            if(!Validation.isEmptyString(input)){
                return input;
            }
        }  
    }
    /**
     * Ask the user to input a String until it is bigger or equal than the minimum size given
     * 
     * @param title String to explain the user what needs to be inputed
     * @param minSize Determines the minimum length for the string
     * @return a non empty String
     */
    static public String askForStringLargeThan(String title, int minSize){
        String input;
        while(true){
            System.out.println(title);
            input = read.nextLine();
            if(Validation.isStringLargerThan(input,minSize)){
                return input;
            }
        }  
    }
    /**
     * Ask the user to input an integer until it is bigger or equal than the minimum size given
     * 
     * @param title String to explain the user what needs to be inputed
     * @param minSize Determines the minimum size for the integer
     * @return a String
     */
    static public int askForIntBiggerThan(String title, int minSize){
        int input;
        while(true){
            System.out.println(title);
            if(read.hasNextInt()){
                input = read.nextInt();
                if(Validation.isIntBiggerThan(input,minSize)){
                    read.nextLine();
                    return input;
                }
            }    
        } 
    }
    /**
     * Ask the user to input a String until it has a valid email structure
     * 
     * @param title String to explain the user what needs to be inputed
     * @return a String that has an email structure
     */
    static public String askForEmail(String title){
        String input;
        while(true){
            System.out.println(title);
            input = read.nextLine();
            if(Validation.isValidEmail(input)){
                return input;
            }
        }
    }
    /**
     * Ask the user to input a String until it has a valid irish phone structure
     * 
     * @param title String to explain the user what needs to be inputed
     * @return a String that has an irish phone structure
     */
    static public String askForPhone(String title){
        String input;
        while(true){
            System.out.println(title);
            input = read.nextLine();
            if(Validation.isValidPhone(input)){
                return input;
            }
        }
    }
    /**
     * Ask the user to input one of the available option in a menu
     * 
     * @param title String to explain the user what needs to be inputed
     * @param numOfOption Number of options available in the active menu
     * @return a valid option of the menu chosen by the user
     */
    static public int askForValidMenuOption(String title, int numOfOption){
        int input;
        while(true){
            System.out.println(title);
            if(read.hasNextInt()){
                input = read.nextInt();
                if(Validation.isValidMenuOption(input,numOfOption)){
                    read.nextLine();
                    return input;
                }
            }
        }
    }
    /**
     * Ask the user to input a valid integer
     * 
     * @param title String to explain the user what needs to be inputed
     * @return an integer
     */
    static public int askForInt(String title){
        int input;
        while(true){
            System.out.println(title);
            if(read.hasNextInt()){
                input = read.nextInt();
                read.nextLine();
                return input;
                
            }
        }
    }
    /**
     * Ask the user to input a valid double
     * 
     * @param title String to explain the user what needs to be inputed
     * @return an double
     */
    static public double askForDouble(String title){
        double input;
        while(true){
            System.out.println(title);
            if(read.hasNextDouble()){
                input = read.nextDouble();
                
                return input;
                
            }
        }
    }
    /**
     * Ask for a positive number to the user
     * 
     * @param title String to explain the user what needs to be inputed
     * @return a positive integer
     */
    static public int askForPositiveInt(String title){
        int input;
        while(true){
            System.out.println(title);
            if(read.hasNextInt()){
                input = read.nextInt();
                if(Validation.isPositive(input)){
                    read.nextLine();
                    return input;
                }
            }
        }
    }
    /**
     * Ask for a positive number to the user
     * 
     * @param title String to explain the user what needs to be inputed
     * @return a positive double
     */
    static public double askForPositiveDouble(String title){
        double input;
        while(true){
            System.out.println(title);
            if(read.hasNextDouble()){
                input = read.nextDouble();
                if(Validation.isPositive(input)){
                    return input;
                }
            }
        }
    }
    /**
     * Ask for a password.
     * the password has to have at lest, an uppercase, a lowercase, a number, a special character
     * @ # $ % 
     * @param title String to explain the user what needs to be inputed
     * @return a valid password
     */
    static public String askForPassword(String title){
        String input;
        while(true){
            System.out.println(title);
            input = read.nextLine();
            if(Validation.isPasswordValid(input)){
                return input;
            }
        }
    }
    /**
     * Ask for a valid date.
     * A valid date is reached when the format is dd/mm/yyyy
     * @param title String to explain the user what needs to be inputed
     * @return a String with a valid date format
     */
    static public String askForDate(String title){
        String input;
        while(true){
            System.out.println(title);
            input = read.nextLine();
            if(Validation.isDateFormatValid(input)){
                return input;
            }
        }
    }
}
