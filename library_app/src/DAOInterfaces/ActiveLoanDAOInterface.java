/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOInterfaces;

import DTO.Loan;
import DTO.User;
import java.util.ArrayList;

/**
 *
 * @author Maria McCahey
 */
public interface ActiveLoanDAOInterface {
    
    public int createLoan(Loan newLoan);
    public Loan getActiveLoan(User activeUser, int book_id, int user_id);
    public int countActiveLoanByBookID(int book_id); 
    public double returnActiveLoan(User activeUser, int user_id, int book_id);
    public int countActiveLoanByCustomerId(int book_id);
    public ArrayList<Loan> seeAllActiveLoans(User activeUser);
    public ArrayList<Loan> seeAllActiveLoansByCustomer(User activeUser,int userId);
}
