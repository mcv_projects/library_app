/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOInterfaces;

import DTO.Loan;
import DTO.User;
import java.util.ArrayList;

/**
 *
 * @author jorgevilasecojurado
 */
public interface LoanRecordDAOInterface {
    public Loan getSingleLoanRecord(User activeUser, int userId, int bookId);
    public int  addNewLoanRecord(Loan newloan, User activeUser);
    public ArrayList<Loan> getAllLoanRecordsByCustomer(User activeUser, int userId);
}
