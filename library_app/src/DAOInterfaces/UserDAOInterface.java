/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOInterfaces;

import DTO.User;

/**
 *
 * @author jorgevilasecojurado
 */
public interface UserDAOInterface {
    
    public int addAdmin(User newUser, User activeUser);
    public User login(User activeUser,String email, String password);
    public double hasUserOverFee(User activeUser);
    public User getUserById(int userId);
    public int increaseUserOverFee(double fee);
    public int removeUser(User activeUser, int userId);
    public int addNewCustomer(User newCustomer);
}
